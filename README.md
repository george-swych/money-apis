# [component name] #

[quick summary of the component]
Type: [JAR, container, .NET, etc.]

### Dependencies ###
* Note any 3rd part libraries, version, and how they can be obtained
* Note any services the component depends on, e.g., PostgreSQL, Spanner, Cloud Pub/Sub
* Note where the configuration information is stored
* Note where credentials are stored; if this is vault, use the full path 
* Note any third party utilities requires to install/configure/run/build this component.

### Configuration ###
* Where are the configuration files located? [provide paths; note the local file as well as the vault path, if there is one] 
* Is this component exposed to the internet? [YES/NO]
* Does this compononent require a static IP?
* What ports does this component listen on? List each and describe.
* Describe, step-by-step, the process for installing and starting the application. Note file locations in full paths, and indicate where all files, scripts, and binaries are. Note what any scripts or files are used for.
* Any "one-off" things: e.g., this is a singleton instance; must be restart q24 hours, etc.

### Repositories ###
* Link any git repos
* Link any artifactory repos for this component's deliverable.

### Security ###
* Annotate any security concerns or details here. (Exposed credentials, administrative ports that shouldn't be exposed to the Internet, etc.)
* Note any service accounts used, as well as where the credentials are stored

### Testing ###
* Describe the steps necessary to setup/install/configure this component so it can be tested. 
* If there are tests in the code base, describe how they can be executed.

### Monitoring ###
How do we check this component to make sure it's healthy?

### Observation ###
What metric can be used to measure the performance of this component?

### Miscellaneous ###
Anything else